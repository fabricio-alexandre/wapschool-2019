<?php

//INICIA CONTAGEM DO TEMPO DE EXECUÇÃO E LEITURA DE ARGUMENTOS
$tempoInicial = microtime(true);

/**
 * Responsavel por exibir o workbench
 * @method workbench
 * @param  float $tempoInicial Tempo do inicio do script
 */
function workbench($tempoInicial){
  $tempoExecucao = number_format((microtime(true) - $tempoInicial), 4);
  echo "Tempo de execução: \033[0;32m".$tempoExecucao." \033[0m(s)".PHP_EOL;
}