<?php
/**
 * $argv[1] = Quantidade de iteraçoes - Valido até 10.000.000
 * $argv[2] = Tipo de teste - Valores válidos (array, objeto)
 */


// INCLUSOES INICIAIS
require_once(__DIR__.'/workbench.php');
require_once(__DIR__.'/system.php');
validarEntradas($argv);


$interacoes = $argv[1];
$tipoTeste  = $argv[2];
$erro       = (isset($argv[3]) and $argv[3] == 'erro');

switch ($tipoTeste) {
  case 'array' : 
    $valores = [
      'soma'          => 1,
      'subtracao'     => 1,
      'multiplicacao' => 1,
      'divisao'       => 1,
    ];
    $valores = testarArray($valores, $interacoes, $erro);
  break;
  
  
  case 'objeto': 
    $valores                = new stdClass;
    $valores->soma          = 1;
    $valores->subtracao     = 1;
    $valores->multiplicacao = 1;
    $valores->divisao       = 1;
    testarObjeto($valores, $interacoes, $erro);
  break;
}
imprimirSaida($tipoTeste, $tempoInicial, $valores, $erro);


/**
 * Realiza os testes com objeto
 * @method testarObjeto
 * @param  stdClass $objValores Objeto contendo os valores
 * @param  integer  $iteracoes  Quantidade de iteracoes
 * @param  bool     $erro       Define se será executado com erro ou nao
 */
function testarObjeto(stdClass $objValores, $iteracoes, $erro){
  $i = 1;
  while ($i <= $iteracoes) {
    $erro ? calcularObjetoComErro($objValores, $i) : calcularObjeto($objValores, $i);
    $i+=1;
  }
}


/**
 * Realiza os calculos com objeto
 * @method calcularObjeto
 * @param  stdClass $objValores
 * @param  integer $calc
 */
function calcularObjeto(stdClass $objValores, $calc){
  $objValores->soma += $calc;
  $objValores->subtracao -= $calc;
  $objValores->multiplicacao *= $calc;
  $objValores->divisao /= $calc;
}


/**
 * Realiza os calculos com array contendo erro
 * @method calcularObjetoComErro
 * @param  stdClass $objValores
 * @param  integer $calc
 */
function calcularObjetoComErro(stdClass $objValores){
  $variavelInvalida = 'string';
  $objValores->soma += $variavelInvalida;
  $objValores->subtracao -= $variavelInvalida;
  $objValores->multiplicacao *= $variavelInvalida;
  $objValores->divisao /= $variavelInvalida;
}


/**
 * Realiza os testes com objeto
 * @method testarArray
 * @param  stdClass $objValores Objeto contendo os valores
 * @param  integer  $iteracoes  Quantidade de iteracoes
 * @param  bool     $erro       Define se será executado com erro ou nao
 */
function testarArray(array $arrayValores, $iteracoes, $erro = false){  
  $i = 1;
  while ($i <= $iteracoes) {
    $arrayValores = $erro ? calcularArrayComErro($arrayValores) : calcularArray($arrayValores, $i);
    $i+=1;
  }
  return $arrayValores;
}


/**
 * Realiza os calculos com array
 * @method calcularArray
 * @param  array   $arrayValores
 * @param  integer $calc
 */
function calcularArray(array $arrayValores, $calc){
  $retorno = array_merge([
    'soma'          => $arrayValores['soma'] += $calc,
    'subtracao'     => $arrayValores['subtracao'] -= $calc,
    'multiplicacao' => $arrayValores['multiplicacao'] *= $calc,
    'divisao'       => $arrayValores['divisao'] /= $calc,
  ]);
  return $retorno;
}


/**
 * Realiza os calculos com array contendo erro
 * @method calcularArrayComErro
 * @param  array   $arrayValores
 * @param  integer $calc
 */
function calcularArrayComErro(array $arrayValores){
  $variavelInvalida = 'string';
  $retorno = [
    'soma'          => $arrayValores['soma'] += $variavelInvalida,
    'subtracao'     => $arrayValores['subtracao'] -= $variavelInvalida,
    'multiplicacao' => $arrayValores['multiplicacao'] *= $variavelInvalida,
    'divisao'       => $arrayValores['divisao'] /= $variavelInvalida,
  ];
  return $retorno;
}