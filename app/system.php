<?php

/**
 * Responsavel por validar as entradas
 * @method validarEntradas
 * @param array $argv
 * @return bool
 */
function validarEntradas($argv = []){
  if (!isset($argv[2]) or !isset($argv[1]))         die('Informe os argumentos de tipo de teste e qtd iterações.'.PHP_EOL.PHP_EOL);
  if (!in_array($argv[2],['array','objeto']))       die('Tipo de teste inválido, tipos aceitos: "array" e "objeto"'.PHP_EOL.PHP_EOL);
  if (!is_numeric($argv[1]) or $argv[1] > 10000000) die('O número máximo de iterações é 1000000'.PHP_EOL.PHP_EOL);
  return true;
}


/**
 * Responsavel por imprimir as saidas no terminal
 * @method imprimirSaida
 * @param  array $saida
 */
function imprimirSaida($tipoTeste, $tempoInicial, $valores, $erro){
  $titulo = 'CALCULANDO COM '.strtoupper($tipoTeste);
  if ($erro) $titulo .= ' (Execução com erros)';

  echo PHP_EOL.PHP_EOL.'*********************************************************';
  echo PHP_EOL."==================== ".$titulo." ====================".PHP_EOL;

  print_r($valores);
  workbench($tempoInicial);
  
  echo PHP_EOL.'=========================================================='.PHP_EOL;
  echo '**********************************************************'.PHP_EOL.PHP_EOL;
}